﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perprog
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Menu();
            
            Console.ReadKey();
        }
        
        static void Menu()
        {
            int selected_menu = 0;
            while (selected_menu != 1 && selected_menu != 2 && selected_menu != 3)
            {
                Console.WriteLine("1 - Titkosítás");
                Console.WriteLine("2 - Visszafejtés");
                Console.WriteLine("3 - Titkosítás és visszafejtés");
                
                selected_menu = int.Parse(Console.ReadLine());
            }
            Start(selected_menu);
        }
        static void Start(int selected_menu)
        {
            if (selected_menu == 1)
            {
                Console.Clear();

                int selected_execution = 0;
                while (selected_execution != 1 && selected_execution != 2)
                {
                    Console.WriteLine("1 - Szekvenciálisan");
                    Console.WriteLine("2 - Párhuzamosan megosztott adatokkal");

                    selected_execution = int.Parse(Console.ReadLine());
                }


                if (selected_execution == 1)
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    Master master = new Master();
                    master.Encode_Seq();

                    sw.Stop();
                    Console.WriteLine();
                    Console.WriteLine("Kész");
                    Console.WriteLine("Futási idő: ");
                    Console.WriteLine(sw.Elapsed.TotalSeconds + " másodperc");
                }
                else
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    Master master = new Master();
                    master.Encode();

                    sw.Stop();
                    Console.WriteLine();
                    Console.WriteLine("Kész");
                    Console.WriteLine("Futási idő: ");
                    Console.WriteLine(sw.Elapsed.TotalSeconds + " másodperc");
                }
            }
            else if (selected_menu == 2)
            {
                Console.Clear();

                int selected_execution = 0;
                while (selected_execution != 1 && selected_execution != 2)
                {
                    Console.WriteLine("1 - Szekvenciálisan");
                    Console.WriteLine("2 - Párhuzamosan megosztott adatokkal");

                    selected_execution = int.Parse(Console.ReadLine());
                }


                if (selected_execution == 1)
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    Master master = new Master();
                    master.Decode_Seq();

                    sw.Stop();
                    Console.WriteLine();
                    Console.WriteLine("Kész");
                    Console.WriteLine("Futási idő: ");
                    Console.WriteLine(sw.Elapsed.TotalSeconds + " másodperc");
                }
                else
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    Master master = new Master();
                    master.Decode();

                    sw.Stop();
                    Console.WriteLine();
                    Console.WriteLine("Kész");
                    Console.WriteLine("Futási idő: ");
                    Console.WriteLine(sw.Elapsed.TotalSeconds + " másodperc");
                }
            }
            else
            {
                Console.Clear();

                int selected_execution = 0;
                while (selected_execution != 1 && selected_execution != 2 && selected_execution != 3)
                {
                    Console.WriteLine("1 - Szekvenciálisan");
                    Console.WriteLine("2 - Párhuzamosan megosztott adatokkal");
                    Console.WriteLine("3 - Párhuzamosan külön adatokkal");

                    selected_execution = int.Parse(Console.ReadLine());
                }


                if (selected_execution == 1)
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    Master master = new Master();
                    master.Encode_Seq();
                    master.Decode_Seq();

                    sw.Stop();
                    Console.WriteLine();
                    Console.WriteLine("Kész");
                    Console.WriteLine("Futási idő: ");
                    Console.WriteLine(sw.Elapsed.TotalSeconds + " másodperc");
                }
                else if(selected_execution == 2)
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    Master master = new Master();
                    master.EncodeAndDecode();

                    sw.Stop();
                    Console.WriteLine();
                    Console.WriteLine("Kész");
                    Console.WriteLine("Futási idő: ");
                    Console.WriteLine(sw.Elapsed.TotalSeconds + " másodperc");
                }
                else
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    Master master = new Master();
                    master.EncodeAndDecodeOwnData();

                    sw.Stop();
                    Console.WriteLine();
                    Console.WriteLine("Kész");
                    Console.WriteLine("Futási idő: ");
                    Console.WriteLine(sw.Elapsed.TotalSeconds + " másodperc");

                }
            }
        }
    }
}
