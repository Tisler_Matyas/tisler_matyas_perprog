﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perprog
{
    class BitmapManager
    {
        public static Color[,] LoadBitmapEncode()
        {
            Bitmap _image = new Bitmap("small.jpg");


            Color[,] _image_color = new Color[_image.Height, _image.Width];

            for (int y = 0; y < _image.Height; y++)
            {
                for (int x = 0; x < _image.Width; x++)
                {
                    _image_color[y, x] = _image.GetPixel(x, y);
                }
            }
            


            return _image_color;
        }
        public static Color[,] LoadBitmapDecode()
        {
            Bitmap _image = new Bitmap("rejtett_kep.jpg");

            Color[,] _image_color = new Color[_image.Height, _image.Width];

            for (int y = 0; y < _image.Height; y++)
            {
                for (int x = 0; x < _image.Width; x++)
                {
                    _image_color[y, x] = _image.GetPixel(x, y);
                }
            }



            return _image_color;
        }
        public static void SaveEncodedBitmap(Color[,] _image_color)
        {
            Bitmap _image = new Bitmap(_image_color.GetLength(1), _image_color.GetLength(0));

            for (int y = 0; y < _image_color.GetLength(1); y++)
            {
                for (int x = 0; x < _image_color.GetLength(0); x++)
                {
                    _image.SetPixel(y, x, _image_color[x, y]);
                }
            }

            _image.Save("rejtett_kep.jpg");
            
        }

        public static void SaveDecodedBitmap(Color[,] _image_color)
        {
            Bitmap _image = new Bitmap(_image_color.GetLength(1), _image_color.GetLength(0));

            for (int y = 0; y < _image_color.GetLength(1); y++)
            {
                for (int x = 0; x < _image_color.GetLength(0); x++)
                {
                    _image.SetPixel(y, x, _image_color[x, y]);
                }
            }

            _image.Save("kikodolt_kep.jpg");

        }
    }
}
