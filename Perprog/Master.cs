﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Perprog
{
    class Master
    {
        int THREADS = Environment.ProcessorCount;

        List<Task> encoder_tasks;
        List<Task> decoder_tasks;
        List<Worker> workers;
        List<Worker_shared_data> workers_shared_data;

        Color[,] colors;
        int[] pixel_archiver;
        
        public Master()
        {
            this.workers = new List<Worker>();
            this.workers_shared_data = new List<Worker_shared_data>();
            this.encoder_tasks = new List<Task>();
            this.decoder_tasks = new List<Task>();


            //Summary();

        }
        public void EncodeAndDecode()
        {
            Encode();
            Decode();
        }
        public void EncodeAndDecodeOwnData()
        {
            LoadPictureEncode();

            pixel_archiver = new int[(colors.GetLength(0) * colors.GetLength(1)) / 2];

            int intervall = pixel_archiver.Length / THREADS;

            for (int i = 0; i < THREADS; i++)
            {
                this.workers.Add(new Worker(i, intervall, colors));
            }

            foreach (Worker worker in workers)
            {
                encoder_tasks.Add(new Task(() =>
                {
                    worker.Encode();
                }, TaskCreationOptions.LongRunning));
            }

            foreach (Task t in encoder_tasks)
            {
                t.Start();
            }

            Task.WaitAll(encoder_tasks.ToArray());
            Summary();
            SaveEncodedPicture();

            foreach (Worker worker in workers)
            {
                decoder_tasks.Add(new Task(() =>
                {
                    worker.Decode();
                }, TaskCreationOptions.LongRunning));
            }

            foreach (Task t in decoder_tasks)
            {
                t.Start();
            }

            Task.WaitAll(decoder_tasks.ToArray());
            Summary();
            SaveDecodedPicture();
        }

        void Encode_CreateWorker_shared_data()
        {
            LoadPictureEncode();

            pixel_archiver = new int[(colors.GetLength(0) * colors.GetLength(1)) / 2];

            int intervall = pixel_archiver.Length / THREADS;

            for (int i = 0; i < THREADS; i++)
            {
                this.workers_shared_data.Add(new Worker_shared_data(i,intervall,colors,pixel_archiver));
            }
            
        }
        void Decode_CreateWorker_shared_data()
        {
            LoadPictureDecode();

            int intervall = pixel_archiver.Length / THREADS;

            for (int i = 0; i < THREADS; i++)
            {
                this.workers_shared_data.Add(new Worker_shared_data(i, intervall, colors, pixel_archiver));
            }

        }
        public void Encode()
        {
            Encode_CreateWorker_shared_data();

            foreach (Worker_shared_data worker_shared_data in workers_shared_data)
            {
                encoder_tasks.Add(new Task(() =>
                {
                    worker_shared_data.Encode();
                }, TaskCreationOptions.LongRunning));
            }

            foreach (Task t in encoder_tasks)
            {
                t.Start();

            }

            Task.WaitAll(encoder_tasks.ToArray());

            SaveEncodedPicture();

        }
        public void Encode_Seq()
        {
            Encode_CreateWorker_shared_data();

            foreach (Worker_shared_data worker_shared_data in workers_shared_data)
            {
                worker_shared_data.Encode();
            }

            //foreach (Worker worker in workers)
            //{
            //    worker.Encode();
            //}

            SaveEncodedPicture();

        }
        public void Decode()
        {
            Decode_CreateWorker_shared_data();

            foreach (Worker_shared_data worker_shared_data in workers_shared_data)
            {
                decoder_tasks.Add(new Task(() =>
                {
                    worker_shared_data.Decode();
                }, TaskCreationOptions.LongRunning));
            }

            foreach (Task t in decoder_tasks)
            {
                t.Start();

            }

            Task.WaitAll(decoder_tasks.ToArray());

            SaveDecodedPicture();

        }
        public void Decode_Seq()
        {
            Decode_CreateWorker_shared_data();

            foreach (Worker_shared_data worker_shared_data in workers_shared_data)
            {
                worker_shared_data.Decode();
            }

            SaveDecodedPicture();

        }
        
        void Summary()
        {
            int temp_i = 0;
            foreach (Worker worker in workers)
            {
                Color[] worker_colors = worker.GetWorkerColors();
                for (int i = 0; i < worker_colors.Length; i++)
                {
                    int y = temp_i / colors.GetLength(1);
                    int x = temp_i - (colors.GetLength(1) * y);
                    colors[y, x] = worker_colors[i];
                    temp_i++;
                }
            }
            foreach (Worker worker in workers)
            {
                Color[] changed_colors = worker.GetChangedColors();
                for (int i = 0; i < changed_colors.Length; i++)
                {
                    int y = temp_i / colors.GetLength(1);
                    int x = temp_i - (colors.GetLength(1) * y);
                    colors[y, x] = changed_colors[i];
                    temp_i++;
                }
            }
            foreach (Worker worker in workers)
            {
                int[] pixel_archiver = worker.GetPixelArchiver();
                for (int i = 0; i < pixel_archiver.Length; i++)
                {
                    this.pixel_archiver[i] = pixel_archiver[i];
                }
            }
        }

        void LoadPictureEncode()
        {
            this.colors = BitmapManager.LoadBitmapEncode();
        }
        void LoadPictureDecode()
        {
            this.colors = BitmapManager.LoadBitmapDecode();
            this.pixel_archiver = ArchiverManager.LoadArchive();
        }

        void SaveEncodedPicture()
        {
            BitmapManager.SaveEncodedBitmap(colors);
            ArchiverManager.SaveArchive(this.pixel_archiver);

            
        }
        void SaveDecodedPicture()
        {
            BitmapManager.SaveDecodedBitmap(colors);
        }
    }
}
