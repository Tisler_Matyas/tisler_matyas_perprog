﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perprog
{
    class Worker
    {
        int index;
        int first_index;
        int last_index;
        Color[,] colors;
        int colors_size;
        List<int> eligible_pixels;
        int width;
        int height;

        int[] pixel_archiver;
        Color[] worker_colors;
        Color[] changed_colors;
        public Worker(int index, int intervall, Color[,] colors)
        {
            this.index = index;
            this.first_index = index * intervall;
            this.last_index = (index + 1) * intervall;
            this.colors = colors;
            this.width = colors.GetLength(1);
            this.height = colors.GetLength(0);
            this.colors_size = colors.GetLength(1) * colors.GetLength(0);
            this.eligible_pixels = new List<int>();
            worker_colors = new Color[last_index - first_index];
            changed_colors = new Color[last_index - first_index];
            pixel_archiver = new int[last_index - first_index];

            int block_counter = 0;
            
            for (int i = first_index; i < last_index; i++)
            {
                int y = i / width;
                int x = i - (width * y);
                worker_colors[block_counter] = colors[y, x];
                block_counter++;
            }

            block_counter = 0;

            for (int i = first_index; i < last_index; i++)
            {
                int temp_i = i + (colors_size / 2);
                int y = temp_i / width;
                int x = temp_i - (width * y);
                changed_colors[block_counter] = colors[y, x];
                block_counter++;
            }

            for (int i = 0; i < changed_colors.Length; i++)
            {
                eligible_pixels.Add(i);
            }
        }


        public void Encode()
        {
            //kodolás
            for (int i = 0; i < worker_colors.Length; i++)
            {
                int random_pixel_index = RandomManager.rnd.Next(eligible_pixels.Count);
                int selected_pixel = eligible_pixels[random_pixel_index];
                eligible_pixels.Remove(selected_pixel);

                pixel_archiver[i] = selected_pixel;

                Color temp_color = worker_colors[i];
                worker_colors[i] = changed_colors[selected_pixel];
                changed_colors[selected_pixel] = temp_color;

            }
        }
        public void Decode()
        {
            //visszafejtés
            for (int i = 0; i < pixel_archiver.Length; i++)
            {
                int selected_pixel = pixel_archiver[i];
                Color temp_color = worker_colors[i];
                worker_colors[i] = changed_colors[selected_pixel];
                changed_colors[selected_pixel] = temp_color;
            }
        }
        

        public Color[] GetWorkerColors()
        {
            return worker_colors;
        }
        public Color[] GetChangedColors()
        {
            return changed_colors;
        }
        public int[] GetPixelArchiver()
        {
            return pixel_archiver;
        }
    }
}
