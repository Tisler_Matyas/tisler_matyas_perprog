﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perprog
{
    class Worker_shared_data
    {
        int index;
        int first_index;
        int last_index;
        Color[,] colors;
        int colors_size;
        List<int> eligible_pixels;
        int width;
        int height;
        int[] pixel_archiver;

        public Worker_shared_data(int index, int intervall, Color[,] colors, int[] pixel_archiver)
        {
            this.index = index;
            this.first_index = index * intervall;
            this.last_index = (index + 1) * intervall;
            this.colors = colors;
            this.width = colors.GetLength(1);
            this.height = colors.GetLength(0);
            this.colors_size = width * height;
            this.eligible_pixels = new List<int>();
            this.pixel_archiver = pixel_archiver;

            for (int i = first_index; i < last_index; i++)
            {
                this.eligible_pixels.Add(i + (colors_size / 2));
            }

        }


        public void Encode()
        {
            for (int i = first_index; i < last_index; i++)
            {
                int random_pixel_index = RandomManager.rnd.Next(eligible_pixels.Count);
                int selected_pixel = eligible_pixels[random_pixel_index];
                int y = selected_pixel / width;
                int x = selected_pixel - (width * y);
                eligible_pixels.Remove(selected_pixel);

                pixel_archiver[i] = selected_pixel;

                int temp_y = i / width;
                int temp_x = i - (width * temp_y);
                Color temp_color = colors[y, x];
                colors[y, x] = colors[temp_y, temp_x];
                colors[temp_y, temp_x] = temp_color;
            }
        }

        public void Decode()
        {
            for (int i = first_index; i < last_index; i++)
            {
                int selected_pixel = pixel_archiver[i];
                int y = selected_pixel / width;
                int x = selected_pixel - (width * y);

                Color temp_color = colors[y, x];

                int temp_y = i / width;
                int temp_x = i - (width * temp_y);

                colors[y, x] = colors[temp_y, temp_x];
                colors[temp_y, temp_x] = temp_color;
            }
        }
    }
}
