﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Perprog
{
    class ArchiverManager
    {
        
        public static int[] LoadArchive()
        {
            int counter = 0;
            StreamReader reader = new StreamReader("archive.txt");
            while (reader.ReadLine() != null)
            {
                counter++;
            }
            reader.Close();

            int[] archive = new int[counter];
            StreamReader reader2 = new StreamReader("archive.txt");

            for (int i = 0; i < archive.Length; i++)
            {
                archive[i] = int.Parse(reader2.ReadLine());
            }

            reader2.Close();

            return archive;
        }
        public static void SaveArchive(int[] archiver)
        {
            StreamWriter writer = new StreamWriter("archive.txt");

            for (int i = 0; i < archiver.Length; i++)
            {
                writer.WriteLine(archiver[i]);
            }
            writer.Dispose();
            writer.Close();
        }
    }
}
